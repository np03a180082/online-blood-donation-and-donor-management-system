from django.apps import AppConfig


class DonorRegistrationConfig(AppConfig):
    name = 'Donor_Registration'

