from django.shortcuts import render
from django.http import HttpResponse
from Donor_Registration.models import Donor
from Contact.models import Contact
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model



# Create your views here.

def index(request):
    return render(request,'index.html',)

def dashboard(request):
    return render(request,'dashboard_base.html',)

def charts(request):
    return render(request,'charts.html',)

def tables(request):
    return render(request,'tables.html',)


def dashboard_view(request):
    users = User.objects.count()
    donor = Donor.objects.count()
    message = Contact.objects.count()
    contact = Contact.objects.all()
    user = User.objects.all()
    context = {"user_count": users, "donor_count": donor, "message_count":message, "contact_list":contact, "user_list":user}
    return render(request, "dashboard_base.html", context)
    # User = get_user_model()
    # count = User.objects.count()
    # return render(request,'dashboard_base.html',{
    #     'count' : count
    # })
    
